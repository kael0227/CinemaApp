﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CinemaAppPro.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace CinemaAppPro.DAL
{
    public class CinemaContext : DbContext
    {
        public CinemaContext() : base("CinemaContext")
        {

        }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Subtitle> Subtitles { get; set; }
        public DbSet<Classification> Classifications { get; set; }
        public DbSet<Protagonist> Protagonists { get; set; } 
        public DbSet<CinemaRoom> CinemaRooms { get; set; }
        public DbSet<Rate> Rates { get; set; } 
        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<MovieToProtagonist> MovieToProtagonist { get; set; }
        public DbSet<CinemaRoomToMovie> CinemaRoomToMovie { get; set; }
        public DbSet<CinemaToCinemaRoom> CinemaToCinemaRoom { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
           


    }

    }
}