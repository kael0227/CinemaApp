﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CinemaRoom
    {
        
        public int CinemaRoomID { get; set; }
        [Display(Name = "Cinema Room")]
        public string CinemaRoomName { get; set; }
        [Display(Name = "Rate")]
        public int RateID { get; set; }
        
        //--------------------------------------------------------------------
        public virtual ICollection<CinemaToCinemaRoom> CinemaToCinemaRoom { get; set; }
        public virtual ICollection<CinemaRoomToMovie> CinemaRoomToMovie { get; set; }

        //---------------------------------------------------------------------
        public virtual Rate Rate { get; set; }
        
    }
}