﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CinemaToCinemaRoom
    {
        public int CinemaToCinemaRoomID { get; set; }
        public int CinemaID { get; set; }
        public int CinemaRoomID { get; set; }
        public virtual Cinema Cinema { get; set; }
        public virtual CinemaRoom CinemaRoom { get; set; }

    }
}