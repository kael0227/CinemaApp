﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Movie
    {
        
        public int MovieID { get; set; }
        [Display(Name = "Movie Name")]
        public string MovieName { get; set; }
        [Display(Name = "Movie Director")]
        public string MovieDirector { get; set; }
        //Relationship One to many-----------------------------------------
        public int LanguageID { get; set; }
        public int GenderID { get; set;  }
        public int SubtitleID { get; set; }
        public int ClassificationID { get; set; }
        //Relationship many to many------------------------------------------
        public virtual ICollection<MovieToProtagonist> MovieToProtagonists { get; set; }
        public virtual ICollection<CinemaRoomToMovie> CinemaRoomToMovie { get; set; }
        // Virtual One to Many -----------------------------------------------

        public virtual Language Language { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual Subtitle Subtitle { get; set; }
        public virtual Classification Classification { get; set; }



    }
}