﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Classification
    {
        public int ClassificationID { get; set; }
        [Display(Name = "Classification Name")]
        public string ClassificationName { get; set; }
        [Display(Name = "Classification Description")]
        public string ClassificationDescription { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}