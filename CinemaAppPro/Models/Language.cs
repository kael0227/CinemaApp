﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Language
    {
        public int LanguageID { get; set; }
        [Display(Name = "Cinema Phone")]
        public string LanguageName { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}