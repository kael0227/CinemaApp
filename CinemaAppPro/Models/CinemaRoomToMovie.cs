﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CinemaRoomToMovie
    {
        public int CinemaRoomToMovieID { get; set; }
        public int CinemaRoomID { get; set; }
        public int MovieID { get; set; }
        public virtual CinemaRoom CinemaRoom { get; set; }
        public virtual Movie Movie { get; set; }
    }
}