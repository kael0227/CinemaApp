﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Cinema
    {
        
        
        public int CinemaID { get; set; }
        [Required]
        [Display(Name = "Cinema Name")]
        public string CinemaName { get; set; }
        [Required(ErrorMessage ="You must provide a PhoneNumber")]
        [Display(Name = "Cinema Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Invalid Phone Number ")]
        public string CinemaTelefono { get; set; }
        [Display(Name = "Cinema Street")]
        public string CinemaCalle { get; set; }
        [Display(Name = "Cinema Sector")]
        public string CinemaSector { get; set; }
        [Display(Name = "Cinema Street Name")]
        public int numero { get; set;  }
        

        //Relacion Meny to meny

        public virtual ICollection<CinemaToCinemaRoom> CinemaToCinemaRoom { get; set; }

    }
}