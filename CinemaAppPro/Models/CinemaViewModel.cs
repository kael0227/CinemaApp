﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CinemaViewModel
    {


        public int CinemaID { get; set; }
        public string CinemaName { get; set; }
        public string CinemaTelefono { get; set; }
        public string CinemaCalle { get; set; }
        public string CinemaSector { get; set; }
        public int numero { get; set; }


        //Relacion Meny to meny

        public List<CheckedBoxViewModel> CinemaRoom { get; set; }

    }
}