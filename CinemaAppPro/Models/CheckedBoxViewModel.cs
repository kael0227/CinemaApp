﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CheckedBoxViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }
}