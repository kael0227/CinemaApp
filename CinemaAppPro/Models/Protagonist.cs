﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Protagonist
    {
        
        public int ProtagonistID { get; set; }
        [Display(Name = "Protagonist Name")]
        public string ProtagonistName { get; set; }
        public virtual ICollection<MovieToProtagonist> MovieToProtagonists { get; set; }
    }
}