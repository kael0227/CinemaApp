﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Subtitle
    {
        public int SubtitleID { get; set; }
        [Display(Name = "Subtitle Name")]
        public string SubtitleName { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}