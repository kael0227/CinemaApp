﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Rate
    {
        public int RateID { get; set; }
        [Display(Name = "Rate Price")]
        public string RatePrice { get; set; }
        [Display(Name = "Day")]
        public string RateDay { get; set; }
        [Display(Name = "Schedule")]
        public string RateSchedule { get; set; }
        public virtual ICollection<CinemaRoom> CinemaRooms { get; set; }
    }
}