﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class Gender
    {
        public int GenderID { get; set; }
        [Display(Name = "Gender Name")]
        public string GenderName { get; set; }
        public virtual ICollection<Movie> Movies { get; set; }
    }
}