﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class CinemaRoomsViewModel
    {
        public int CinemaRoomID { get; set; }
        public string CinemaRoomName { get; set; }
        public int RateID { get; set; }
        //--------------------------------------------------------------------
        
        public List<CheckedBoxViewModel> Movie { get; set; }

        //---------------------------------------------------------------------
        public virtual Rate Rate { get; set; }
    }
}