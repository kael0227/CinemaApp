﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinemaAppPro.Models
{
    public class MovieToProtagonist
    {
        public int MovieToProtagonistID { get; set; }
        public int MovieID { get; set; }
        public int ProtagonistID { get; set; }
        public virtual Movie Movie {get;set;}
        public virtual Protagonist Protagonist { get; set; }

    }
}