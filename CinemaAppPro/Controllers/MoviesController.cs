﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaAppPro.DAL;
using CinemaAppPro.Models;

namespace CinemaAppPro.Controllers
{
    public class MoviesController : Controller
    {
        private CinemaContext db = new CinemaContext();

        // GET: Movies
        public ActionResult Index()
        {
            var movies = db.Movies.Include(m => m.Classification).Include(m => m.Gender).Include(m => m.Language).Include(m => m.Subtitle);
            return View(movies.ToList());
        }

        // GET: Movies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            var results = from p in db.Protagonists
                          select new
                          {
                              p.ProtagonistID,
                              p.ProtagonistName,
                              Checked = ((from ap in db.MovieToProtagonist
                                          where (ap.MovieID == id) & (ap.ProtagonistID == p.ProtagonistID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new MoviesViewModel();

            MyViewModel.MovieID = id.Value;
            MyViewModel.MovieName = movie.MovieName;
            MyViewModel.MovieDirector = movie.MovieDirector;
            MyViewModel.Language = movie.Language;
            MyViewModel.Subtitle = movie.Subtitle;
            MyViewModel.Gender = movie.Gender;
            MyViewModel.Classification = movie.Classification;
            var MyCheckBoxList = new List<CheckedBoxViewModel>();

            foreach (var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.ProtagonistID, Name = item.ProtagonistName, Checked = item.Checked });
            };
            MyViewModel.Protagonist = MyCheckBoxList;
            return View(MyViewModel);
        }

        // GET: Movies/Create
        public ActionResult Create()
        {
            ViewBag.ClassificationID = new SelectList(db.Classifications, "ClassificationID", "ClassificationName");
            ViewBag.GenderID = new SelectList(db.Genders, "GenderID", "GenderName");
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName");
            ViewBag.SubtitleID = new SelectList(db.Subtitles, "SubtitleID", "SubtitleName");
            return View();
        }

        // POST: Movies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MovieID,MovieName,MovieDirector,LanguageID,GenderID,SubtitleID,ClassificationID")] Movie movie)
        {
            if (ModelState.IsValid)
            {
                db.Movies.Add(movie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassificationID = new SelectList(db.Classifications, "ClassificationID", "ClassificationName", movie.ClassificationID);
            ViewBag.GenderID = new SelectList(db.Genders, "GenderID", "GenderName", movie.GenderID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", movie.LanguageID);
            ViewBag.SubtitleID = new SelectList(db.Subtitles, "SubtitleID", "SubtitleName", movie.SubtitleID);
            return View(movie);
        }

        // GET: Movies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassificationID = new SelectList(db.Classifications, "ClassificationID", "ClassificationName", movie.ClassificationID);
            ViewBag.GenderID = new SelectList(db.Genders, "GenderID", "GenderName", movie.GenderID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", movie.LanguageID);
            ViewBag.SubtitleID = new SelectList(db.Subtitles, "SubtitleID", "SubtitleName", movie.SubtitleID);
            var results = from p in db.Protagonists
                          select new
                          {
                              p.ProtagonistID,
                              p.ProtagonistName,
                              Checked = ((from ap in db.MovieToProtagonist
                                          where (ap.MovieID == id) & (ap.ProtagonistID == p.ProtagonistID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new MoviesViewModel();

            MyViewModel.MovieID = id.Value;
            MyViewModel.MovieName = movie.MovieName;
            MyViewModel.MovieDirector = movie.MovieDirector;
            MyViewModel.LanguageID = movie.LanguageID;
            MyViewModel.SubtitleID = movie.SubtitleID;
            MyViewModel.GenderID = movie.GenderID;
            MyViewModel.ClassificationID = movie.ClassificationID;
            var MyCheckBoxList = new List<CheckedBoxViewModel>();
            
            foreach(var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.ProtagonistID, Name = item.ProtagonistName, Checked = item.Checked });
            };
            MyViewModel.Protagonist = MyCheckBoxList;
            return View(MyViewModel);
        }

        // POST: Movies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MoviesViewModel movie)
        {
            if (ModelState.IsValid)
            {
                var MyMovie = db.Movies.Find(movie.MovieID);
                MyMovie.MovieName = movie.MovieName;
                MyMovie.MovieDirector = movie.MovieDirector;
                MyMovie.LanguageID = movie.LanguageID;
                MyMovie.SubtitleID = movie.SubtitleID;
                MyMovie.GenderID = movie.GenderID;
                MyMovie.ClassificationID = movie.ClassificationID;
                foreach (var item in db.MovieToProtagonist)
                {
                    if(item.MovieID == movie.MovieID)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    }
                }
                foreach (var item in movie.Protagonist)
                {
                    if (item.Checked)
                    {
                        db.MovieToProtagonist.Add(new MovieToProtagonist() { MovieID = movie.MovieID, ProtagonistID = item.ID });
                    }
                }


                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassificationID = new SelectList(db.Classifications, "ClassificationID", "ClassificationName", movie.ClassificationID);
            ViewBag.GenderID = new SelectList(db.Genders, "GenderID", "GenderName", movie.GenderID);
            ViewBag.LanguageID = new SelectList(db.Languages, "LanguageID", "LanguageName", movie.LanguageID);
            ViewBag.SubtitleID = new SelectList(db.Subtitles, "SubtitleID", "SubtitleName", movie.SubtitleID);
            return View(movie);
        }

        // GET: Movies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movie movie = db.Movies.Find(id);
            db.Movies.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
