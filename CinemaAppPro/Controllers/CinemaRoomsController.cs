﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaAppPro.DAL;
using CinemaAppPro.Models;

namespace CinemaAppPro.Controllers
{
    public class CinemaRoomsController : Controller
    {
        private CinemaContext db = new CinemaContext();

        // GET: CinemaRooms
        public ActionResult Index()
        {
            var cinemaRooms = db.CinemaRooms.Include(c => c.Rate);
            return View(cinemaRooms.ToList());
        }

        // GET: CinemaRooms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CinemaRoom cinemaRoom = db.CinemaRooms.Find(id);
            if (cinemaRoom == null)
            {
                return HttpNotFound();
            }
            var results = from p in db.Movies
                          select new
                          {
                              p.MovieID,
                              p.MovieName,
                              Checked = ((from ap in db.CinemaRoomToMovie
                                          where (ap.CinemaRoomID == id) & (ap.MovieID == p.MovieID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new CinemaRoomsViewModel();

            MyViewModel.CinemaRoomID = id.Value;
            MyViewModel.CinemaRoomName = cinemaRoom.CinemaRoomName;
            MyViewModel.Rate = cinemaRoom.Rate;
            var MyCheckBoxList = new List<CheckedBoxViewModel>();

            foreach (var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.MovieID, Name = item.MovieName, Checked = item.Checked });
            };
            MyViewModel.Movie = MyCheckBoxList;
            return View(MyViewModel);
        }

        // GET: CinemaRooms/Create
        public ActionResult Create()
        {
            ViewBag.RateID = new SelectList(db.Rates, "RateID", "RatePrice");
            return View();
        }

        // POST: CinemaRooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CinemaRoomID,CinemaRoomName,RateID")] CinemaRoom cinemaRoom)
        {
            if (ModelState.IsValid)
            {
                db.CinemaRooms.Add(cinemaRoom);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RateID = new SelectList(db.Rates, "RateID", "RatePrice", cinemaRoom.RateID);
            return View(cinemaRoom);
        }

        // GET: CinemaRooms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CinemaRoom cinemaRoom = db.CinemaRooms.Find(id);
            if (cinemaRoom == null)
            {
                return HttpNotFound();
            }
            ViewBag.RateID = new SelectList(db.Rates, "RateID", "RatePrice", cinemaRoom.RateID);
            var results = from p in db.Movies
                          select new
                          {
                              p.MovieID,
                              p.MovieName,
                              Checked = ((from ap in db.CinemaRoomToMovie
                                          where (ap.CinemaRoomID == id) & (ap.MovieID == p.MovieID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new CinemaRoomsViewModel();

            MyViewModel.CinemaRoomID = id.Value;
            MyViewModel.CinemaRoomName = cinemaRoom.CinemaRoomName;
            MyViewModel.RateID = cinemaRoom.RateID;
            var MyCheckBoxList = new List<CheckedBoxViewModel>();

            foreach (var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.MovieID, Name = item.MovieName, Checked = item.Checked });
            };
            MyViewModel.Movie = MyCheckBoxList;
            return View(MyViewModel);
        }

        // POST: CinemaRooms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CinemaRoomsViewModel cinemaRoom)
        {
            if (ModelState.IsValid)
            {
                var MyMovie = db.CinemaRooms.Find(cinemaRoom.CinemaRoomID);
                MyMovie.CinemaRoomName = cinemaRoom.CinemaRoomName;
                MyMovie.RateID = cinemaRoom.RateID;
                foreach (var item in db.CinemaRoomToMovie)
                {
                    if (item.CinemaRoomID == cinemaRoom.CinemaRoomID)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    }
                }
                foreach (var item in cinemaRoom.Movie)
                {
                    if (item.Checked)
                    {
                        db.CinemaRoomToMovie.Add(new CinemaRoomToMovie() { CinemaRoomID = cinemaRoom.CinemaRoomID, MovieID = item.ID });
                    }
                }


                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RateID = new SelectList(db.Rates, "RateID", "RatePrice", cinemaRoom.RateID);
            return View(cinemaRoom);
        }

        // GET: CinemaRooms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CinemaRoom cinemaRoom = db.CinemaRooms.Find(id);
            if (cinemaRoom == null)
            {
                return HttpNotFound();
            }
            return View(cinemaRoom);
        }

        // POST: CinemaRooms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CinemaRoom cinemaRoom = db.CinemaRooms.Find(id);
            db.CinemaRooms.Remove(cinemaRoom);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
