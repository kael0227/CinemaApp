﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaAppPro.DAL;
using CinemaAppPro.Models;

namespace CinemaAppPro.Controllers
{
    public class CinemasController : Controller
    {
        private CinemaContext db = new CinemaContext();

        // GET: Cinemas
        public ActionResult Index()
        {
            return View(db.Cinemas.ToList());
        }

        // GET: Cinemas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cinema cinema = db.Cinemas.Find(id);
            if (cinema == null)
            {
                return HttpNotFound();
            }
            var results = from p in db.CinemaRooms
                          select new
                          {
                              p.CinemaRoomID,
                              p.CinemaRoomName,
                              Checked = ((from ap in db.CinemaToCinemaRoom
                                          where (ap.CinemaID == id) & (ap.CinemaRoomID == p.CinemaRoomID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new CinemaViewModel();

            MyViewModel.CinemaID = id.Value;
            MyViewModel.CinemaName = cinema.CinemaName;
            MyViewModel.CinemaTelefono = cinema.CinemaTelefono;
            MyViewModel.CinemaCalle = cinema.CinemaCalle;
            MyViewModel.CinemaSector = cinema.CinemaSector;
            MyViewModel.numero = cinema.numero;

            var MyCheckBoxList = new List<CheckedBoxViewModel>();

            foreach (var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.CinemaRoomID, Name = item.CinemaRoomName, Checked = item.Checked });
            };
            MyViewModel.CinemaRoom = MyCheckBoxList;
            return View(MyViewModel);
        }

        // GET: Cinemas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cinemas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CinemaID,CinemaName,CinemaTelefono,CinemaCalle,CinemaSector,numero")] Cinema cinema)
        {
            if (ModelState.IsValid)
            {
                db.Cinemas.Add(cinema);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cinema);
        }

        // GET: Cinemas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cinema cinema = db.Cinemas.Find(id);
            if (cinema == null)
            {
                return HttpNotFound();
            }
            var results = from p in db.CinemaRooms
                          select new
                          {
                              p.CinemaRoomID,
                              p.CinemaRoomName,
                              Checked = ((from ap in db.CinemaToCinemaRoom
                                          where (ap.CinemaID == id) & (ap.CinemaRoomID == p.CinemaRoomID)
                                          select ap).Count() > 0)
                          };
            var MyViewModel = new CinemaViewModel();

            MyViewModel.CinemaID = id.Value;
            MyViewModel.CinemaName = cinema.CinemaName;
            MyViewModel.CinemaTelefono = cinema.CinemaTelefono;
            MyViewModel.CinemaCalle = cinema.CinemaCalle;
            MyViewModel.CinemaSector = cinema.CinemaSector;
            MyViewModel.numero = cinema.numero;
            

            var MyCheckBoxList = new List<CheckedBoxViewModel>();

            foreach (var item in results)
            {
                MyCheckBoxList.Add(new CheckedBoxViewModel { ID = item.CinemaRoomID, Name = item.CinemaRoomName, Checked = item.Checked });
            };
            MyViewModel.CinemaRoom = MyCheckBoxList;
            return View(MyViewModel);
        }

        // POST: Cinemas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CinemaViewModel cinema)
        {
            if (ModelState.IsValid)
            {
                var MyMovie = db.Cinemas.Find(cinema.CinemaID);
                MyMovie.CinemaName = cinema.CinemaName;
                MyMovie.CinemaTelefono = cinema.CinemaTelefono;
                MyMovie.CinemaCalle = cinema.CinemaCalle;
                MyMovie.CinemaSector = cinema.CinemaSector;
                MyMovie.numero = cinema.numero;

                foreach (var item in db.CinemaToCinemaRoom)
                {
                    if (item.CinemaID == cinema.CinemaID)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    }
                }
                foreach (var item in cinema.CinemaRoom)
                {
                    if (item.Checked)
                    {
                        db.CinemaToCinemaRoom.Add(new CinemaToCinemaRoom() { CinemaID = cinema.CinemaID, CinemaRoomID = item.ID });
                    }
                }


                db.SaveChanges();
                return RedirectToAction("Index");
            }
                return View(cinema);
        }

        // GET: Cinemas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cinema cinema = db.Cinemas.Find(id);
            if (cinema == null)
            {
                return HttpNotFound();
            }
            return View(cinema);
        }

        // POST: Cinemas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Cinema cinema = db.Cinemas.Find(id);
            db.Cinemas.Remove(cinema);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
