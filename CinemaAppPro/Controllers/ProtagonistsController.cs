﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CinemaAppPro.DAL;
using CinemaAppPro.Models;

namespace CinemaAppPro.Controllers
{
    public class ProtagonistsController : Controller
    {
        private CinemaContext db = new CinemaContext();

        // GET: Protagonists
        public ActionResult Index()
        {
            return View(db.Protagonists.ToList());
        }

        // GET: Protagonists/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Protagonist protagonist = db.Protagonists.Find(id);
            if (protagonist == null)
            {
                return HttpNotFound();
            }
            return View(protagonist);
        }

        // GET: Protagonists/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Protagonists/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProtagonistID,ProtagonistName")] Protagonist protagonist)
        {
            if (ModelState.IsValid)
            {
                db.Protagonists.Add(protagonist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(protagonist);
        }

        // GET: Protagonists/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Protagonist protagonist = db.Protagonists.Find(id);
            if (protagonist == null)
            {
                return HttpNotFound();
            }
            return View(protagonist);
        }

        // POST: Protagonists/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProtagonistID,ProtagonistName")] Protagonist protagonist)
        {
            if (ModelState.IsValid)
            {
                db.Entry(protagonist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(protagonist);
        }

        // GET: Protagonists/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Protagonist protagonist = db.Protagonists.Find(id);
            if (protagonist == null)
            {
                return HttpNotFound();
            }
            return View(protagonist);
        }

        // POST: Protagonists/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Protagonist protagonist = db.Protagonists.Find(id);
            db.Protagonists.Remove(protagonist);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
